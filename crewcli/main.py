import os
import json

from invoke import Collection, Program
from invoke.config import Config, merge_dicts

from .utils import print_green_bg, print_magenta, print_red_bg
from . import tasks


def cli_run(c, cmd):
    print_magenta(cmd)
    # warn=True will not throw an exception when executing commands that return a
    # status code other than 0
    output = c.run(cmd, warn=True)
    if output.ok:
        print_green_bg("Success")
    else:
        print_red_bg("Error")
    return output


class DataConfig(Config):
    @staticmethod
    def global_defaults():

        try:
            cwd = os.getcwd()
            config_path = os.path.join(cwd, "config")
            project_data_path = os.path.join(config_path, "project.json")
            secrets_data_path = os.path.join(config_path, "secrets.json")
            with open(secrets_data_path) as f:
                secrets_data = json.load(f)
            with open(project_data_path) as f:
                project_data = json.load(f)
        except FileNotFoundError:
            # We may be in the backend folder, so try again one level up
            cwd = os.path.dirname(os.getcwd())
            config_path = os.path.join(cwd, "config")
            project_data_path = os.path.join(config_path, "project.json")
            secrets_data_path = os.path.join(config_path, "secrets.json")
            with open(secrets_data_path) as f:
                secrets_data = json.load(f)
            with open(project_data_path) as f:
                project_data = json.load(f)

        data = {
            "project_data": project_data,
            "secrets_data": secrets_data,
            "cli_run": cli_run,
            "current_dir": cwd,
        }

        base_defaults = Config.global_defaults()

        return merge_dicts(base_defaults, data)


program = Program(
    config_class=DataConfig, namespace=Collection.from_module(tasks), version="0.0.57"
)
