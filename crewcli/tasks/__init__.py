from invoke import Collection

from crewcli.tasks import cloudfront, db, deploy, dynos, misc, provision, s3

ns = Collection()

ns.add_task(cloudfront.create_distribution)

ns.add_task(db.upgrade_heroku_db)
ns.add_task(db.copy_heroku_db)
ns.add_task(db.schedule_heroku_backups)
ns.add_task(db.reset_heroku_db)
ns.add_task(db.download_heroku_db)
ns.add_task(db.restore_dump)
ns.add_task(db.create_local_db)
ns.add_task(db.delete_local_db)
ns.add_task(db.reset_local_db)

ns.add_task(deploy.deploy)
ns.add_task(deploy.deploy_all)
ns.add_task(deploy.m_on)
ns.add_task(deploy.m_off)

ns.add_task(dynos.enable_preboot)
ns.add_task(dynos.enable_metadata)
ns.add_task(dynos.ps)
ns.add_task(dynos.logs)
ns.add_task(dynos.scale_dynos)
ns.add_task(dynos.scale_workers)
ns.add_task(dynos.heroku_usage)

ns.add_task(misc.open_site)
ns.add_task(misc.delete_pyc)
ns.add_task(misc.generate_secret_key)
ns.add_task(misc.delete_all_migrations)

ns.add_task(provision.add_remote)
ns.add_task(provision.provision_apps)
ns.add_task(provision.create_app)
ns.add_task(provision.set_vars)
ns.add_task(provision.add_buildpacks)
ns.add_task(provision.create_pipeline_with_app)
ns.add_task(provision.add_app_to_pipeline)
ns.add_task(provision.provision_addons)

ns.add_task(s3.create_bucket)
ns.add_task(s3.create_all_buckets)
ns.add_task(s3.list_buckets)
ns.add_task(s3.get_media)
ns.add_task(s3.put_media)
ns.add_task(s3.set_bucket_cors)
