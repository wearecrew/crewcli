import json
import os

import boto3
from invoke import task
from colorama import Fore

from ..utils import print_magenta_bg


@task
def create_distribution(c, environment):

    print_magenta_bg("Create Cloudfront distribution")

    curr_dir = os.path.dirname(os.path.abspath(__file__))
    parent_dir = os.path.dirname(curr_dir)
    file_path = os.path.join(parent_dir, "config", "origin-distribution.json")

    with open(file_path, "r") as cloudfront_config_file:
        cloudfront_config = cloudfront_config_file.read()

    domain = c.project_data["environments"][environment]["domain"]
    aws_profile_name = c.project_data["common"]["aws_profile_name"]

    cloudfront_config_with_domain = cloudfront_config.replace("{{ ORIGIN }}", domain)
    cloudfront_config_with_domain = json.loads(cloudfront_config_with_domain)

    boto3.setup_default_session(profile_name=aws_profile_name)
    client = boto3.client("cloudfront")
    response = client.create_distribution(DistributionConfig=cloudfront_config_with_domain)
    distribution = response["Distribution"]["DomainName"]
    distribution_id = response["Distribution"]["Id"]

    print(Fore.MAGENTA + f"Distribution {distribution_id} created at {distribution}" + Fore.RESET)
    print(
        Fore.RED
        + "Now waiting for the distribution to be deployed, so we can set the Heroku ORIGIN_DISTRIBUTION environment variable."
        + " This may take up to 20 mins, please do not quit this task until then"
        + Fore.RESET
    )

    waiter = client.get_waiter("distribution_deployed")
    waiter.wait(Id=response["Distribution"]["Id"])

    cmd = f"heroku config:add ORIGIN_DISTRIBUTION='https://{distribution}' --remote {environment}"
    c.cli_run(c, cmd)
