import os
import re

from invoke import task

from ..utils import print_magenta_bg, print_red


@task
def upgrade_heroku_db(c, environment):
    print_magenta_bg("Upgrade DB")

    # Create new
    cmd = f"heroku addons:create heroku-postgresql:hobby-basic --remote {environment}"
    create_result = c.cli_run(c, cmd)
    heroku_url_pattern = re.compile(r"(HEROKU_[A-Z_]+)")
    create_search = heroku_url_pattern.search(create_result.stdout)
    new_db_url = create_search.group()

    # Copy old to new
    app_name = c.project_data["environments"][environment]["heroku_app_name"]
    cmd = f"heroku pg:copy DATABASE_URL {new_db_url} --remote {environment} --confirm {app_name}"
    c.cli_run(c, cmd)

    # Promote new
    cmd = f"heroku pg:promote {new_db_url} --remote {environment}"
    promote_result = c.cli_run(c, cmd)
    promote_search = heroku_url_pattern.search(promote_result.stderr)
    old_db_url = promote_search.group()

    # Remove old
    cmd = f"heroku addons:destroy {old_db_url} --remote {environment} --confirm {app_name}"
    c.cli_run(c, cmd)


@task
def copy_heroku_db(c, from_app, to_app):
    print_magenta_bg("Copy database")
    print_red(f"This will copy data from {from_app} to {to_app}, type {to_app} to continue")
    val = input()
    if val == to_app:
        cmd = f"heroku pg:copy {from_app}::DATABASE_URL DATABASE_URL -a {to_app} --confirm {to_app}"
        c.cli_run(c, cmd)
    else:
        print_red("Aborted")


@task
def schedule_heroku_backups(c, environment):
    print_magenta_bg("Schedule backups")

    cmd = f'heroku pg:backups:schedule --at "02:00 Europe/London" --remote {environment}'
    c.cli_run(c, cmd)


@task
def reset_heroku_db(c, environment):
    print_magenta_bg("Reset DB")
    cmd = f"heroku pg:reset DATABASE_URL --remote {environment}"
    c.cli_run(c, cmd)


@task
def download_heroku_db(c, environment):
    print_magenta_bg("Download DB")

    # Remove old database file
    path = os.path.join(c.current_dir, "latest.dump")
    cmd = f"rm -f {path}"
    c.cli_run(c, cmd)

    # Capture Heroku backup
    cmd = f"heroku pg:backups:capture --remote {environment}"
    c.cli_run(c, cmd)

    # Download data from Heroku
    cmd = f"heroku pg:backups:download --remote {environment}"
    c.cli_run(c, cmd)


@task
def restore_dump(c):
    print_magenta_bg("Restore DB")

    create_local_db(c)
    db_name = c.project_data["environments"]["local"]["database_name"]
    cmd = (
        "pg_restore --verbose --clean --no-acl --no-owner -h "
        f"localhost -U djangouser -d {db_name} latest.dump"
    )
    c.cli_run(c, cmd)


@task
def create_local_db(c):
    print_magenta_bg("Create local DB")

    db_name = c.project_data["environments"]["local"]["database_name"]
    cmd = f"psql --command 'CREATE DATABASE {db_name}'"
    c.cli_run(c, cmd)

    cmd = f"psql --command 'GRANT ALL ON DATABASE {db_name} to djangouser'"
    c.cli_run(c, cmd)


@task
def delete_local_db(c):
    print_magenta_bg("Delete local DB")

    db_name = c.project_data["environments"]["local"]["database_name"]
    cmd = f"psql --command 'DROP DATABASE {db_name}'"
    c.cli_run(c, cmd)


@task
def reset_local_db(c):
    print_magenta_bg("Reset local DB")

    delete_local_db(c)
    create_local_db(c)
