from invoke import task

from ..utils import print_magenta_bg


@task()
def deploy(c, environment, branch):
    print_magenta_bg("Deploy")
    if branch is None:
        raise ValueError("Please specify a branch eg. $crewcli deploy staging master")
    cmd = f"git push --force {environment} {branch}:master"
    c.cli_run(c, cmd)


@task
def deploy_all(c, branch):
    print_magenta_bg("Deploy all")
    for environment in c["project_data"]["environments"]:
        if environment != "local":
            deploy(c, environment, branch)


@task
def m_on(c, environment):
    print_magenta_bg("Maintenance on")
    cmd = "heroku maintenance:on --remote {environment}".format(environment=environment)
    c.cli_run(c, cmd)


@task
def m_off(c, environment):
    print_magenta_bg("Maintenance off")
    cmd = "heroku maintenance:off --remote {environment}".format(environment=environment)
    c.cli_run(c, cmd)
