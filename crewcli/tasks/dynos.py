import re

from invoke import task

from crewcli.utils import print_magenta_bg

from ..utils import print_blue_bg, print_magenta_bg, print_blue


@task
def enable_preboot(c, environment):
    print_magenta_bg("Enable preboot")
    cmd = f"heroku features:enable --remote {environment} preboot"
    c.cli_run(c, cmd)


@task
def enable_metadata(c, environment):
    print_magenta_bg("Enable metadata")
    cmd = f"heroku labs:enable runtime-dyno-metadata --remote {environment}"
    c.cli_run(c, cmd)


@task
def ps(c, environment):
    print_magenta_bg("PS")
    cmd = f"heroku ps --remote {environment}"
    c.cli_run(c, cmd)


@task
def logs(c, environment):
    print_magenta_bg("Logs")
    cmd = f"heroku logs --remote {environment}"
    c.cli_run(c, cmd)


@task
def scale_dynos(c, environment, dynos_required):
    print_magenta_bg("Scale dynos")
    if dynos_required is None:
        raise ValueError("Please specify number of dynos eg. $invoke scale-dynos development 1")

    cmd = f"heroku ps:scale web={dynos_required} --remote {environment}"
    c.cli_run(c, cmd)


@task
def scale_workers(c, environment, dynos_required):
    print_magenta_bg("Scale workers")
    if dynos_required is None:
        raise ValueError("Please specify number of workers eg. $invoke scale-workers development 1")

    cmd = f"heroku ps:scale workers={dynos_required} --remote {environment}"
    c.cli_run(c, cmd)


@task
def heroku_usage(c):
    print_magenta_bg("List running dynos")
    team_name = c.project_data["common"]["heroku_team_name"]
    cmd = f"heroku apps --team {team_name}"
    result = c.cli_run(c, cmd)
    dyno_results = []
    db_results = []
    for result_line in result.stdout.splitlines():
        if "===" not in result_line and result_line != "":
            app_name = re.sub(r"\(\S+\)", "", result_line)
            cmd = f"heroku apps:info {app_name}"
            result = c.cli_run(c, cmd)
            dyno_matches = re.search(r"^Dynos:\s+web: (\d)", result.stdout, re.MULTILINE)
            if dyno_matches is not None:
                dyno_results.append(app_name)
            db_matches = re.search(r"heroku-postgresql:(\S+)$", result.stdout, re.MULTILINE)
            if db_matches is not None:
                db_results.append(f"{app_name} : {db_matches.group(0)}")

    print_magenta_bg("Apps running dynos")
    for dyno_result in dyno_results:
        print_blue(dyno_result)

    print_magenta_bg("App DBs")
    for db_result in db_results:
        print_blue(db_result)
