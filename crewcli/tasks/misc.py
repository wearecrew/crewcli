import os
import random

from invoke import task

from ..utils import print_magenta_bg, print_green


@task
def open_site(c, environment):
    print_magenta_bg("Open site")
    cmd = f"heroku open --remote {environment}"
    c.cli_run(c, cmd)


@task
def delete_pyc(c):
    print_magenta_bg("Delete pyc files")
    cmd = "find . -name '*.pyc' -delete"
    c.cli_run(c, cmd)


@task
def generate_secret_key(c):
    print_magenta_bg("Generate secret key")
    result = "".join(
        [random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)]
    )
    print_green(result)


@task
def delete_all_migrations(c):
    print_magenta_bg("Reset all migrations")

    path = os.path.join(c.current_dir, "backend")

    cmd = f"find . -path '{path}/*/migrations/*.py' -not -name '__init__.py' -delete"
    c.cli_run(c, cmd)
    cmd = f"find . -path '{path}/*/migrations/*.pyc' -delete"
    c.cli_run(c, cmd)
