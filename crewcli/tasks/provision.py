from invoke import task

from .deploy import deploy
from .db import schedule_heroku_backups, upgrade_heroku_db
from .s3 import put_media, create_bucket, set_bucket_cors

from ..utils import print_magenta_bg, print_blue_bg


@task
def add_remote(c, environment):
    print_magenta_bg("Add remote")
    app_name = c.project_data["environments"][environment]["heroku_app_name"]
    cmd = f"heroku git:remote --app {app_name} --remote {environment}"
    c.cli_run(c, cmd)


@task
def provision_apps(c):
    print_blue_bg("Provision apps")
    for environment in c.project_data["environments"]:
        if environment != "local":
            create_app(c, environment=environment)
            add_remote(c, environment=environment)
            add_buildpacks(c, environment=environment)
            set_vars(c, environment=environment)

            deploy(c, branch="master", environment=environment)

            create_bucket(c, environment=environment)
            set_bucket_cors(c, environment=environment)
            put_media(c, environment=environment)

            if environment == "development":
                create_pipeline_with_app(c, environment)
            else:
                add_app_to_pipeline(c, environment)

            provision_addons(c, environment)

            if environment == "production":
                upgrade_heroku_db(c, environment=environment)
                schedule_heroku_backups(c, environment=environment)


@task
def create_app(c, environment):
    print_magenta_bg("Create app")
    app_name = c.project_data["environments"][environment]["heroku_app_name"]
    team_name = c.project_data["common"]["heroku_team_name"]
    region = c.project_data["common"]["heroku_region"]
    cmd = f"heroku create {app_name} --remote {environment} --team {team_name} --region {region}"
    c.cli_run(c, cmd)


@task
def set_vars(c, environment):
    print_magenta_bg("Set vars")

    # Set ENV to development, staging, production
    cmd = f"heroku config:add ENV='{environment}' --remote {environment}"
    c.cli_run(c, cmd)

    # Set DJANGO_SETTINGS_MODULE
    cmd = f"heroku config:add DJANGO_SETTINGS_MODULE='project.settings.production' --remote {environment}"
    c.cli_run(c, cmd)

    # Set common secrets
    if hasattr(c.secrets_data, "common"):
        for key, value in c.secrets_data["common"].items():
            cmd = f"heroku config:add {key}='{value}' --remote {environment}"
            c.cli_run(c, cmd)

    # Set environment specific secrets
    if hasattr(c.secrets_data, "environments"):
        for key, value in c.secrets_data["environments"][environment].items():
            cmd = f"heroku config:add {key}='{value}' --remote {environment}"
            c.cli_run(c, cmd)


@task
def add_buildpacks(c, environment):
    print_magenta_bg("Add buildpacks")

    buildpacks = c.project_data["common"]["heroku_buildpacks"]

    for index, buildpack in enumerate(buildpacks):
        index1 = index + 1
        cmd = f"heroku buildpacks:add --index {index1} {buildpack} --remote {environment}"
        c.cli_run(c, cmd)


@task
def create_pipeline_with_app(c, environment):
    print_magenta_bg("Create pipeline")

    pipeline_name = c.project_data["common"]["heroku_pipeline_name"]
    team_name = c.project_data["common"]["heroku_team_name"]
    cmd = (
        f"heroku pipelines:create {pipeline_name} --remote {environment}"
        f" --stage {environment} --team {team_name}"
    )
    c.cli_run(c, cmd)


@task
def add_app_to_pipeline(c, environment):
    print_magenta_bg("Add app to pipeline")

    pipeline_name = c.project_data["common"]["heroku_pipeline_name"]
    cmd = f"heroku pipelines:add {pipeline_name} --remote {environment} --stage {environment}"
    c.cli_run(c, cmd)


@task
def provision_addons(c, environment):
    print_magenta_bg("Provision addons")
    addons = c.project_data["environments"][environment]["heroku_addons"]
    for addon in addons:
        name = addon["name"]
        plan = addon["plan"]
        cmd = f"heroku addons:create {name}:{plan} --remote {environment}"
        c.cli_run(c, cmd)
