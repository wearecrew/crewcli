import os

from invoke import task

from ..utils import print_magenta_bg, print_blue_bg


@task
def create_bucket(c, environment):
    print_magenta_bg("Create bucket")
    bucket_name = c.project_data["environments"][environment]["s3_bucket_name"]
    aws_profile_name = c.project_data["common"]["aws_profile_name"]
    cmd = f"aws s3 mb s3://{bucket_name} --profile {aws_profile_name}"
    c.cli_run(c, cmd)


@task
def create_all_buckets(c):
    print_blue_bg("Create all buckets")
    for environment in c.project_data["environments"]:
        if environment != "local":
            create_bucket(c, environment=environment)
            set_bucket_cors(c, environment=environment)


@task
def list_buckets(c):
    print_magenta_bg("List buckets")
    aws_profile_name = c.project_data["common"]["aws_profile_name"]
    cmd = f"aws s3 ls --profile {aws_profile_name}"
    c.cli_run(c, cmd)


@task
def get_media(c, environment):
    print_magenta_bg("Get media")
    bucket_name = c.project_data["environments"][environment]["s3_bucket_name"]
    aws_profile_name = c.project_data["common"]["aws_profile_name"]
    src = f"s3://{bucket_name}/"
    dest = os.path.join(c.current_dir, "backend", "media")
    cmd = f"aws s3 sync {src} {dest} --profile {aws_profile_name}"
    c.cli_run(c, cmd)


@task
def put_media(c, environment):
    print_magenta_bg("Put media")
    bucket_name = c.project_data["environments"][environment]["s3_bucket_name"]
    aws_profile_name = c.project_data["common"]["aws_profile_name"]
    src = os.path.join(c.current_dir, "backend", "media")
    dest = f"s3://{bucket_name}/"
    cmd = f"aws s3 sync {src} {dest} --acl public-read --profile {aws_profile_name}"
    c.cli_run(c, cmd)


@task
def set_bucket_cors(c, environment):
    print_magenta_bg("Set bucket CORS")
    bucket_name = c.project_data["environments"][environment]["s3_bucket_name"]
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    parent_dir = os.path.dirname(curr_dir)
    file_path = os.path.join(parent_dir, "config", "cors.json")
    aws_profile_name = c.project_data["common"]["aws_profile_name"]
    cmd = (
        f"aws s3api put-bucket-cors --bucket {bucket_name}"
        f" --cors-configuration file://{file_path} --profile {aws_profile_name}"
    )
    c.cli_run(c, cmd)
