from colorama import Fore, Back


def print_magenta(output):
    print(f"{Fore.MAGENTA}------- {output} -------{Fore.RESET}")


def print_green(output):
    print(f"{Fore.GREEN}------- {output} -------{Fore.RESET}")


def print_red(output):
    print(f"{Fore.RED}------- {output} -------{Fore.RESET}")


def print_blue(output):
    print(f"{Fore.BLUE}------- {output} -------{Fore.RESET}")


def print_magenta_bg(output):
    print(f"{Fore.WHITE}{Back.MAGENTA}------- {output} -------{Back.RESET}{Fore.RESET}")


def print_green_bg(output):
    print(f"{Fore.WHITE}{Back.GREEN}------- {output} -------{Back.RESET}{Fore.RESET}")


def print_red_bg(output):
    print(f"{Fore.WHITE}{Back.RED}------- {output} -------{Back.RESET}{Fore.RESET}")


def print_blue_bg(output):
    print(f"{Fore.WHITE}{Back.BLUE}------- {output} -------{Back.RESET}{Fore.RESET}")
