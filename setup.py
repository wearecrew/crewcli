from setuptools import setup

setup(
    name="crewcli",
    version="0.0.57",
    packages=["crewcli"],
    install_requires=["colorama", "invoke==1.3.0", "boto3"],
    entry_points={"console_scripts": ["crew = crewcli.main:program.run"]},
)
